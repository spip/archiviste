<?php

/**
 * SPIP, Système de publication pour l'internet
 *
 * Copyright © avec tendresse depuis 2001
 * Arnaud Martin, Antoine Pitrou, Philippe Rivière, Emmanuel Saint-James
 *
 * Ce programme est un logiciel libre distribué sous licence GNU/GPL.
 */

/**
 * @deprecated 3.0 use Spip\Archiver\SpipArchiver
 */

namespace Spip\Archives;

include_spip('src/ArchiverInterface');
include_spip('src/AbstractArchiver');
include_spip('src/ArchiveInterface');
include_spip('src/NoDotFilterIterator');
include_spip('src/TarArchive');
include_spip('src/TgzArchive');
include_spip('src/ZipArchive');
include_spip('src/SpipArchiver');
trigger_error('inc/archives is deprecated. use Spip\Archiver\SpipArchiver instead.', \E_USER_DEPRECATED);

use Spip\Archiver\SpipArchiver;

/**
 * Point d'entrée de la gestion des archives compressées de SPIP jusqu'à la version 2.2
 *
 * @deprecated 3.0 use Spip\Archiver\SpipArchiver
 */
class SpipArchives extends SpipArchiver
{
	public function __construct(string $fichier_archive, string $mode_compression = '') {
		trigger_error(
			'Spip\Archives\SpipArchives is deprecated. use Spip\Archiver\SpipArchiver instead.',
			\E_USER_DEPRECATED
		);
		parent::__construct($fichier_archive, $mode_compression);
	}
}
