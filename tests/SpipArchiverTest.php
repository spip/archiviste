<?php

namespace Spip\Test\Archiver;

use PHPUnit\Framework\Attributes\DataProvider;
use Spip\Archiver\SpipArchiver;

/**
 * @covers \Spip\Archiver\AbstractArchiver
 * @covers \Spip\Archiver\SpipArchiver
 * @covers \Spip\Archiver\ZipArchive
 * @covers \Spip\Archiver\TarArchive
 * @covers \Spip\Archiver\TgzArchive
 * @covers \Spip\Archiver\NoDotFilterIterator
 *
 * @internal
 */
class SpipArchiverTest extends TestCase
{
	public static function setUpBeforeClass(): void
	{
		self::hardClean();
		self::createFile();
		self::createFile('/dir/sub_dir/test.txt');
		self::$directoriesToClean[] = self::getBaseDir() . '/dir';
		self::createFile('/commenter3.exotic');

		// setup Zip
		self::createZip('/test.zip', [
			'directory/test.txt' => 'contenu de test',
		]);
		self::createZip('/commenter.zip', [
			'directory/test.txt' => 'contenu de test',
		]);
		self::createZip('/commenter2.zip', [
			'directory/test.txt' => 'contenu de test',
		]);
		$test_commenter_zip2 = new \ZipArchive();
		$test_commenter_zip2->open(self::getBaseDir() . '/commenter2.zip');
		$test_commenter_zip2->setArchiveComment('A beautiful comment');
		$test_commenter_zip2->close();
		self::createZip('/retirer.zip', [
			'test.txt' => 'contenu de test',
			'sub_directory/test2.txt' => 'contenu de test2',
		]);

		// setup Tar
		self::createFile('/tar/directory/test.txt');
		self::$directoriesToClean[] = self::getBaseDir() . '/tar';
		self::createTar();
		self::createTar('/tar/retirer.tar', [
			'test.txt' => 'contenu de test',
			'sub_directory/test2.txt' => 'contenu de test2',
		]);

		// setup Tgz
		self::createFile('/tgz/directory/test.txt');
		self::createTgz();
	}

	public static function tearDownAfterClass(): void
	{
		self::clean();
	}

	public static function dataInformer()
	{
		return [
			'empty-string' => [
				3,
				[
					'proprietes' => [],
					'fichiers' => [],
					'commentaire' => '',
				],
				'',
				'',
			],
			'unknown' => [
				2,
				[
					'proprietes' => [],
					'fichiers' => [],
					'commentaire' => '',
				],
				self::getBaseDir() . '/commenter3.exotic',
				'',
			],
			'exotic' => [
				2,
				[
					'proprietes' => [],
					'fichiers' => [],
					'commentaire' => '',
				],
				self::getBaseDir() . '/commenter3.exotic',
				'exotic',
			],
			'not-zip' => [
				1,
				[
					'proprietes' => [],
					'fichiers' => [],
					'commentaire' => '',
				],
				self::getBaseDir() . '/commenter3.exotic',
				'zip',
			],
			'zip' => [
				0,
				[
					'proprietes' => [
						'racine' => 'directory/',
					],
					'fichiers' => [
						[
							'filename' => 'directory/test.txt',
							'size' => 15,
						],
					],
					'commentaire' => ''
				],
				self::getBaseDir() . '/test.zip',
				'',
			],
			'zip-with-comment' => [
				0,
				[
					'proprietes' => [
						'racine' => 'directory/',
					],
					'fichiers' => [
						[
							'filename' => 'directory/test.txt',
							'size' => 15,
						],
					],
					'commentaire' => 'A beautiful comment'
				],
				self::getBaseDir() . '/commenter2.zip',
				'',
			],
			'tar' => [
				0,
				[
					'proprietes' => [
						'racine' => '/directory/',
					],
					'fichiers' => [
						[
							'filename' => '/directory/test.txt',
							'size' => 15,
						],
					],
					'commentaire' => ''
				],
				self::getBaseDir() . '/tar/test.tar',
				'',
			],
			'tgz' => [
				0,
				[
					'proprietes' => [
						'racine' => '/directory/',
					],
					'fichiers' => [
						[
							'filename' => '/directory/test.txt',
							'size' => 15,
						],
					],
					'commentaire' => ''
				],
				self::getBaseDir() . '/tgz/test.tar.gz',
				'',
			],
		];
	}

	#[DataProvider('dataInformer')]
	public function testInformer($expected, $expectedList, $file, $extension)
	{
		// Given
		$archiver = new SpipArchiver($file, $extension);

		// When
		$actual = $archiver->informer();

		//Then
		$this->assertEquals($expectedList, $actual);
		$this->assertEquals($expected, $archiver->erreur());
	}

	public static function dataDeballer()
	{
		return [
			'zip' => [
				true,
				0,
				self::getBaseDir() . '/test.zip',
				'',
				self::getBaseDir(),
				[],
				self::getBaseDir() . '/directory/test.txt',
			],
			'destination-not-exists' => [
				false,
				5,
				self::getBaseDir() . '/tmp/test.zip',
				'',
				'',
				[self::getBaseDir() . '/directory/test.txt'],
				self::getBaseDir() . '/directory' . md5(random_int(0, mt_getrandmax())),
			],
			'tar' => [
				true,
				0,
				self::getBaseDir() . '/tar/test.tar',
				'',
				self::getBaseDir() . '/tar',
				[],
				self::getBaseDir() . '/tar/directory/test.txt',
			],
			'tgz' => [
				true,
				0,
				self::getBaseDir() . '/tgz/test.tar.gz',
				'',
				self::getBaseDir() . '/tgz',
				['directory/test.txt'],
				self::getBaseDir() . '/tgz/directory/test.txt',
			],
			'not-zip' => [
				false,
				1,
				self::getBaseDir() . '/directory/test.txt',
				'zip',
				self::getBaseDir(),
				[],
				null,
			],
		];
	}

	#[DataProvider('dataDeballer')]
	public function testDeballer($expected, $expectedErreur, $file, $extension, $target, $files, $testFile)
	{
		// Given
		@\unlink($testFile);
		$archiver = new SpipArchiver($file, $extension);

		// When
		$actual = $archiver->deballer($target, $files);

		//Then
		$this->assertEquals($expected, $actual, 'decompression ko');
		$this->assertEquals($expectedErreur, $archiver->erreur(), 'wrong error code');
		if ($expectedErreur === 0) {
			$this->assertFileExists($testFile);
		}
	}

	public static function dataEmballer()
	{
		return [
			'destination-not-exists' => [
				false,
				4,
				self::getBaseDir() . '/not-writable/fichier.zip',
				'',
				[self::getBaseDir() . '/directory/test.txt' => 'directory/test.txt'],
				self::getBaseDir() . '/directory',
			],
			'archive-exists' => [
				false,
				6,
				self::getBaseDir() . '/test.zip',
				'',
				[self::getBaseDir() . '/directory/test.txt' => 'directory/test.txt'],
				null,
			],
			'source-not-exists' => [
				false,
				7,
				self::getBaseDir() . '/dummy.zip',
				'',
				[self::getBaseDir() . '/directory/test.txt'],
				self::getBaseDir() . '/directory' . md5(random_int(0, mt_getrandmax())),
			],
			'zip' => [
				true,
				0,
				self::getBaseDir() . '/emballer.zip',
				'',
				[self::getBaseDir() . '/directory/test.txt'],
				self::getBaseDir() . '/directory',
			],
			'zip2' => [
				true,
				0,
				self::getBaseDir() . '/emballer2.zip',
				'',
				[self::getBaseDir() . '/directory/test.txt'],
				null,
			],
			'zip3' => [
				true,
				0,
				self::getBaseDir() . '/emballer3.zip',
				'',
				[self::getBaseDir() . '/directory' => 'directory'],
				null,
			],
			'zip4' => [
				true,
				0,
				self::getBaseDir() . '/emballer4.zip',
				'',
				[self::getBaseDir() . '/directory' => 'test'],
				null,
			],
			'zip5' => [
				true,
				0,
				self::getBaseDir() . '/emballer5.zip',
				'',
				[self::getBaseDir() . '/dir'],
				null,
			],
			'tar' => [
				true,
				0,
				self::getBaseDir() . '/tar/emballer.tar',
				'',
				[self::getBaseDir() . '/directory/test.txt'],
				self::getBaseDir() . '/directory',
			],
			'tar2' => [
				true,
				0,
				self::getBaseDir() . '/tar/emballer2.tar',
				'',
				[self::getBaseDir() . '/directory/test.txt' => 'flat.txt'],
				self::getBaseDir() . '/directory',
			],
		];
	}

	#[DataProvider('dataEmballer')]
	public function testEmballer($expected, $expectedErreur, $file, $extension, $files, $target)
	{
		// Given
		$archiver = new SpipArchiver($file, $extension);

		// When
		$actual = $archiver->emballer($files, $target);

		//Then
		$this->assertEquals($expected, $actual, 'compression ko');
		$this->assertEquals($expectedErreur, $archiver->erreur(), 'error code');
		self::$filesToClean[] = $file;
	}

	public static function dataRetirer()
	{
		return [
			'not-exists' => [
				false,
				3,
				md5(random_int(0, mt_getrandmax())),
				'zip',
				['test.txt'],
			],
			'zip' => [
				true,
				0,
				self::getBaseDir() . '/retirer.zip',
				'',
				['test.txt'],
			],
			'vider' => [
				false,
				8,
				self::getBaseDir() . '/retirer.zip',
				'',
				['test.txt', 'sub_directory/test2.txt'],
			],
			'tar' => [
				true,
				0,
				self::getBaseDir() . '/tar/retirer.tar',
				'',
				['directory/test.txt'],
			],
		];
	}

	#[DataProvider('dataRetirer')]
	public function testRetirer($expected, $expectedErreur, $file, $extension, $files)
	{
		// Given
		$archiver = new SpipArchiver($file, $extension);

		// When
		$actual = $archiver->retirer($files);

		//Then
		$this->assertEquals($expected, $actual, 'remove ok');
		$this->assertEquals($expectedErreur, $archiver->erreur(), 'error code');
	}

	public static function dataCommenter()
	{
		return [
			'bad-zip' => [
				false,
				2,
				[
					'proprietes' => [],
					'fichiers' => [],
					'commentaire' => '',
				],
				self::getBaseDir() . '/commenter3.exotic',
				'zip',
				'test',
			],
			'zip' => [
				true,
				0,
				[
					'proprietes' => [
						'racine' => 'directory/',
					],
					'fichiers' => [
						[
							'filename' => 'directory/test.txt',
							'size' => 15,
						],
					],
					'commentaire' => 'A beautiful comment'
				],
				self::getBaseDir() . '/commenter.zip',
				'',
				'A beautiful comment',
			],
			'zip-edit' => [
				true,
				0,
				[
					'proprietes' => [
						'racine' => 'directory/',
					],
					'fichiers' => [
						[
							'filename' => 'directory/test.txt',
							'size' => 15,
						],
					],
					'commentaire' => 'An edited comment'
				],
				self::getBaseDir() . '/commenter.zip',
				'zip',
				'An edited comment',
			],
		];
	}

	#[DataProvider('dataCommenter')]
	public function testCommenter($expected, $expectedErreur, $expectedInformer, $file, $extension, $comment)
	{
		// Given
		$archiver = new SpipArchiver($file, $extension);

		// When
		$actual = $archiver->commenter($comment);

		//Then
		$this->assertEquals($expected, $actual, 'comment ok');
		$this->assertEquals($expectedErreur, $archiver->erreur(), 'error code ok');
		$this->assertEquals($expectedInformer, $archiver->informer(), 'error informer ok');
	}
}
