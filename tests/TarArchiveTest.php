<?php

namespace Spip\Test\Archiver;

use Spip\Archiver\TarArchive;

/**
 * @covers Spip\Archiver\TarArchive
 * @covers Spip\Archiver\NoDotFilterIterator
 */
class TarArchiveTest extends TestCase
{
	public static function setUpBeforeClass(): void
	{
		self::hardClean();
		self::createFile();
		self::createTar();
	}

	public static function tearDownAfterClass(): void
	{
		self::clean();
	}

	public function testCompressTriggered()
	{
		// Given
		$buffer = [];
		$previous = \set_error_handler(function ($errno, $errstr, $errfile, $errline) use (&$buffer) {
			$buffer[] = $errstr;
			return true;
		}, E_USER_DEPRECATED);
		$archive = new TarArchive();

		// When
		$archive->compress('test', []);

		// Then
		$this->assertEquals(['source parameter is deprecated.'], $buffer);
		\set_error_handler($previous);
	}

	public function testCompress()
	{
		$archive = new TarArchive();
		$archive->open(self::getBaseDir() . '/emballer.tar', 'creation');
		$compress = $archive->compress('', [
			self::getBaseDir() . '/directory/test.txt' =>  '/directory/test.txt',
		]);
		$archive->close();

		$this->assertEquals(true, $compress, 'compress ok');
		self::$filesToClean[] = self::getBaseDir() . '/emballer.tar';
	}

	public function testList()
	{
		$archive = new TarArchive();
		$archive->open(self::getBaseDir() . '/tar/test.tar', 'lecture');
		$actual = $archive->list();
		$archive->close();

		$this->assertEquals(
			'/directory/test.txt',
			self::relativePath($actual[0]['filename']),
			'archive should contain a file \'directory/test.txt\''
		);
	}

	public function testExtractTo()
	{
		@\unlink(self::getBaseDir() . '/directory/test.txt');
		$archive = new TarArchive();
		$archive->open(self::getBaseDir() . '/tar/test.tar', 'lecture');
		$extracted = $archive->extractTo(self::getBaseDir());

		$this->assertTrue($extracted, 'extractTo ok');
		$this->assertFileExists(self::getBaseDir() . '/directory/test.txt');
		$this->assertEquals(
			'contenu de test',
			\file_get_contents(self::getBaseDir() . '/directory/test.txt'),
			''
		);
	}

	public function testRemove()
	{
		$archive = new TarArchive();
		$archive->open(self::getBaseDir() . '/tar/test.tar', 'edition');
		$remove = $archive->remove(['directory/test.txt']);
		$actualList = $archive->list();
		$archive->close();

		$this->assertTrue($remove);
		$this->assertEquals([], $actualList);
	}
}
