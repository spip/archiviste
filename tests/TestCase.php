<?php

namespace Spip\Test\Archiver;

use PHPUnit\Framework\TestCase as FrameworkTestCase;

class TestCase extends FrameworkTestCase
{
	protected static $filesToClean = [];
	protected static $directoriesToClean = [];

	protected static function getBaseDir(): string
	{
		return sys_get_temp_dir() . DIRECTORY_SEPARATOR . 'archiviste';
	}

	public static function relativePath(string $path): string
	{
		return str_replace(self::getBaseDir() . '/', '', $path);
	}

	public static function createFile(
		string $filename = '/directory/test.txt',
		?string $content = null,
	): string {
		$filename = self::getBaseDir() . $filename;
		$directory = dirname($filename);
		@\mkdir($directory, recursive: true);
		@\file_put_contents($filename, $content ?? 'contenu de test');
		self::$filesToClean[] = $filename;
		self::$directoriesToClean[] = $directory;

		return $filename;
	}

	public static function createZip(
		string $filename = '/test.zip',
		?array $files = null,
		?string $comment = null,
	): string {
		if (empty($files)) {
			$files = [self::relativePath(self::createFile()) => 'contenu de test'];
		}
		$filename = self::getBaseDir() . $filename;
		$directory = dirname($filename);
		@\mkdir($directory, recursive: true);

		$zip = new \ZipArchive();
		$zip->open($filename, \ZipArchive::CREATE);
		foreach ($files as $file => $content) {
			if (\file_exists($file)) {
				$zip->addFile($file);
			} else {
				$zip->addFromString($file, $content);
			}
		}
		if (!empty($comment)) {
			$zip->setArchiveComment($comment);
		}
		$zip->close();
		self::$filesToClean[] = $filename;
		if ($directory !== self::getBaseDir()) {
			self::$directoriesToClean[] = $directory;
		}

		return $filename;
	}

	public static function createTar(
		string $filename = '/tar/test.tar',
		?array $files = null,
	): string {
		if (empty($files)) {
			$files = ['directory/test.txt' => 'contenu de test'];
		}
		$filename = self::getBaseDir() . $filename;
		$directory = dirname($filename);
		@\mkdir($directory, recursive: true);

		$tar = new \PharData(
			$filename,
			\FilesystemIterator::SKIP_DOTS | \FilesystemIterator::UNIX_PATHS,
			null,
			\Phar::TAR
		);
		foreach ($files as $file => $content) {
			if (\file_exists($file)) {
				$tar->addFile($file);
			} else {
				$tar->addFromString($file, $content);
			}
		}
		self::$filesToClean[] = $filename;
		if ($directory !== self::getBaseDir()) {
			self::$directoriesToClean[] = $directory;
		}

		return $filename;
	}

	public static function createTgz(
		string $filename = '/tgz/test.tar.gz',
		?array $files = null,
	): string {
		if (empty($files)) {
			$files = ['directory/test.txt' => 'contenu de test'];
		}
		$filename = self::getBaseDir() . $filename;
		$directory = dirname($filename);
		@\mkdir($directory, recursive: true);

		$tar = new \PharData(
			$filename,
			\FilesystemIterator::SKIP_DOTS | \FilesystemIterator::UNIX_PATHS,
			null,
			\Phar::GZ
		);
		foreach ($files as $file => $content) {
			if (\file_exists($file)) {
				$tar->addFile($file);
			} else {
				$tar->addFromString($file, $content);
			}
		}
		self::$filesToClean[] = $filename;
		if ($directory !== self::getBaseDir()) {
			self::$directoriesToClean[] = $directory;
		}

		return $filename;
	}

	public static function clean(): void
	{
		foreach (array_unique(self::$filesToClean) as $file) {
			@\unlink($file);
		}

		$dirs = array_unique(self::$directoriesToClean);
		rsort($dirs);
		foreach ($dirs as $directory) {
			@\rmdir($directory);
		}
	}

	public static function hardClean(): void
	{
		if (\is_dir(self::getBaseDir())) {
			$cleaner = new \RecursiveIteratorIterator(
				new \RecursiveDirectoryIterator(self::getBaseDir())
			);
			foreach ($cleaner as $file) {
				@\unlink($file->getPathname());
			}
		}
	}
}
