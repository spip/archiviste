<?php

namespace Spip\Test\Archiver;

use PHPUnit\Framework\Attributes\DataProvider;
use Spip\Archiver\ZipArchive;

/**
 * @covers Spip\Archiver\ZipArchive
 */
class ZipArchiveTest extends TestCase
{
	public static function setUpBeforeClass(): void
	{
		self::hardClean();
		self::createFile();
		self::createZip();
		self::createZip('/remove.zip', [
			'directory/test.txt' => 'contenu de test',
			'directory/test2.txt' => 'contenu de test2',
		]);
	}

	public static function tearDownAfterClass(): void
	{
		self::clean();
	}

	public function testCompressTriggered()
	{
		// Given
		$buffer = [];
		$previous = \set_error_handler(function ($errno, $errstr, $errfile, $errline) use (&$buffer) {
			$buffer[] = $errstr;
			return true;
		}, E_USER_DEPRECATED);
		$archive = new ZipArchive();

		// When
		$archive->compress('test', []);

		// Then
		$this->assertEquals(['source parameter is deprecated.'], $buffer);
		\set_error_handler($previous);
	}

	public function testCompress()
	{
		$archive = new ZipArchive();
		$archive->open(self::getBaseDir() . '/emballer.zip', 'creation');
		$compress = $archive->compress('', [
			self::getBaseDir() . '/directory/test.txt' => 'directory/test.txt',
		]);
		$archive->close();

		$this->assertTrue($compress, 'compress ok');
		self::$filesToClean[] = self::getBaseDir() . '/emballer.zip';
	}

	public function testList()
	{
		$archive = new ZipArchive();
		$archive->open(self::getBaseDir() . '/test.zip', 'lecture');
		$actual = $archive->list();
		$archive->close();

		$this->assertEquals(
			'directory/test.txt',
			self::relativePath($actual[0]['filename']),
			'archive should contain a file \'directory/test.txt\''
		);
	}

	public function testExtractTo()
	{
		$archive = new ZipArchive();
		$archive->open(self::getBaseDir() . '/test.zip', 'lecture');
		$extracted = $archive->extractTo(self::getBaseDir());

		$this->assertTrue($extracted, 'extractTo ok');
		$this->assertFileExists(self::getBaseDir() . '/directory/test.txt');
		$this->assertEquals(
			'contenu de test',
			\file_get_contents(self::getBaseDir() . '/directory/test.txt'),
			'extracted file should contain \'contenu de test\''
		);
	}

	public function testSetComment()
	{
		$archive = new ZipArchive();
		$archive->open(self::getBaseDir() . '/test.zip', 'edition');
		$defaultComment = $archive->getComment();
		$comment = $archive->setComment('A beautiful comment');
		$archive->close();

		$archive->open(self::getBaseDir() . '/test.zip', 'edition');
		$extractedComment = $archive->getComment();
		$archive->close();

		$this->assertTrue($comment);
		$this->assertEquals('', $defaultComment);
		$this->assertEquals('A beautiful comment', $extractedComment);
	}

	public static function dataRemove()
	{
		return [
			'one-file' => [
				'expected' => true,
				'fileToRemove' => ['directory/test.txt'],
			],
			'wrong-file' => [
				'expected' => false,
				'fileToRemove' => ['directory/test3.txt'],
			],
			'two-files' => [
				'expected' => false,
				'fileToRemove' => ['directory/test.txt', 'directory/test2.txt'],
			],
		];
	}

	#[DataProvider('dataRemove')]
	public function testRemove($expected, $filesToRemove)
	{
		$archive = new ZipArchive();
		$archive->open(self::getBaseDir() . '/remove.zip', 'edition');
		$actual = $archive->remove($filesToRemove);
		$archive->close();

		$this->assertEquals($expected, $actual);
	}
}
