# Plugin Archiviste

Ce plugin SPIP fournit une API pour générer ou décompresser des archives (zip, tar)

À partir de la version 3.0, il pourra être installé en tant que package composer.

Il y a donc 2 manières de l'utiliser :

Historiquement, avec la fonction SPIP `include_spip()` :

```php
<?php

use Spip\Archives\SpipArchiches
include_spip('inc/archives');

// ...

$archive = new SpipArchives($fichier);
```

Quand composer pourra être utilisé pour installer des packages dans SPIP[^1] :

```php
<?php

use Spip\Archiver\SpipArchiver

// ...

$archive = new SpipArchiver($fichier);
```

L'objet `$archive` répond à l'interface [ArchiverInterface](src/ArchiverInterface.php)

## Contribution

Cette librairie est développée avec les outils PHP_CodeSniffer, PHPStan et PHPUnit.

Vous pouvez vérifier que vos contributions n'introduisent pas de régressions avant de soumettre vos propositions de changement :

```bash
composer install
# Respect des coding standards SPIP
vendor/bin/ecs
vendor/bin/rector --dry-run
# Analyse statique
XDEBUG_MODE=off vendor/bin/phpstan
# Tests Unitaires (et calcul du taux de couverture avec Xdebug)
XDEBUG_MODE=coverage vendor/bin/phpunit --colors
```

Fichier à consulter après l'exécution des  commandes ci-dessus :

- `.phpunit.cache/html/index.html`

## Notes

[^1] : l'archive spip classique (par exemple, [SPIP 4.2.2](https://files.spip.net/spip/archives/spip-v4.2.2.zip)) intègre l'autoloader composer. Il est donc présent lors d'une mise à jour ou une installation via [spip_loader](https://www.spip.net/fr_article5705.html). Si vous avez installé SPIP  via `git`, n'oubliez pas d'exécuter la commande `composer install` (ou `composer install --no-dev`), à la racine de votre projet SPIP. voir [Composer](https://getcomposer.org/doc/00-intro.md).
