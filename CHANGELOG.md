# Changelog

## 3.0.0 - 2025-11-25

### Changed

- Correction clés de langue retourné par les messages d’erreur
- Chaines de langue dans le nouveau format
- Gestion des retours des méthodes ZipArchive::open, SpipArchiver::commenter et SpipArchiver::retirer
- `Spip\Archiver\ArchiverInterface::emballer()` accepte comme premier paramètre, soit une liste de fichiers, soit un tableau associatif du type `['source' => 'destination']`, auquel cas le second paramètre n'est pas pris en compte.

### Removed

- Abandon du support pour PHP7.4 & PHP8.0
- Abandon du support du dépôt git spip/tests

### Deprecated

- appel `include_spip('inc/archives')`
- classe `Spip\Archives\SpipArchives`
